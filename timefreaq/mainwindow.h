#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QFileDialog>
#include "mythread.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QFileDialog fileDialog;
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_startButton_clicked();
    void on_stopButton_clicked();
    void timer1_work();

    void on_saveAsPushButton_clicked();

    void on_scanSerialPortsButton_clicked();

public slots:
    void threadFinished();
    void getThreadMessage(QString str);

signals:
    void finishThread();

private:
    Ui::MainWindow *ui;
    QTimer *timer1;
    MyThread mythread;
    void setWidgetsEnabled(bool enabled);
    void scanSerialPorts();
    void setupListWidget();
    void setupFileDialog(const QString &directory);
};

#endif // MAINWINDOW_H
