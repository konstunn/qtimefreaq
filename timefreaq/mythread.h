#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QThread>
#include <QListWidget>

class MyThread : public QThread
{
    Q_OBJECT
private:
    bool shouldFinish;
    QString srPortName;
    QString vchPortName;
    QListWidget *listWidget;
    int vchOutChanNum = 1;
    QString outFilename;
public:
    explicit MyThread(QObject *parent = 0);
    void run();

    void setComPortNames(QString srPortName, QString vchPortName);
    void setVchChannels(QListWidget *listWidget, int vchOutChannel);
    void setOutFilename(QString outFilename);

protected:

signals:
    void iAmRunning(QString str);

public slots:
    void finish();
};

#endif // MYTHREAD_H
