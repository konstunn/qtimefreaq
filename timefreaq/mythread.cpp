#include "mythread.h"

MyThread::MyThread(QObject *parent) :
    QThread(parent)
{
}

void MyThread::run()
{
    const int N = 5;
    const int msecs = 500;
    shouldFinish = false;

    QListWidgetItem *item;

    while (!shouldFinish) {
        for (int i = 1; i <= N; ++i) {
            emit iAmRunning(tr("thread: im running %1\n").arg(i));
            emit iAmRunning(tr("srPortName: %1\n").arg(this->srPortName));
            emit iAmRunning(tr("vchPortName: %1\n").arg(this->vchPortName));
            emit iAmRunning(tr("outFilename: %1\n").arg(this->outFilename));
            emit iAmRunning(tr("vchOutChanNum: %1\n").arg(this->vchOutChanNum));

            emit iAmRunning(tr("vch input channels: "));
            for (int j = 0; j < this->listWidget->count(); ++j) {
                item = listWidget->item(j);
                if (item->checkState() == Qt::Checked)
                    emit iAmRunning(QString::number(j+1) + tr(" "));
            }
            emit iAmRunning(tr("\n"));

            msleep(msecs);
        }
    }
}

void MyThread::finish()
{
    shouldFinish = true;
}

void MyThread::setComPortNames(QString srPort, QString vchPort)
{
    this->srPortName = srPort;
    this->vchPortName = vchPort;
}

void MyThread::setVchChannels(QListWidget *inChannels, int outChannel)
{
    this->listWidget = inChannels;
    this->vchOutChanNum = outChannel;
}

void MyThread::setOutFilename(QString filename)
{
    this->outFilename = filename;
}
