#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>

#include <QSerialPort>
#include <QSerialPortInfo>

#include <QMessageBox>

#include <QFileDialog>
#include <QStandardPaths>

#include <QDateTime>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // add 10 checked channel items to list widget

    setupListWidget();

    // enumerate serial ports and display them in comboboxes
    scanSerialPorts();

    // create timer and connect a slot to it
    timer1 = new QTimer(this);
    timer1->setSingleShot(false);
    connect(timer1, SIGNAL(timeout()), this, SLOT(timer1_work()));

    // connect slots
    connect(&mythread, SIGNAL(finished()), this, SLOT(threadFinished()));
    connect(this, SIGNAL(finishThread()), &mythread, SLOT(finish()));
    connect(&mythread, SIGNAL(iAmRunning(QString)), this,
            SLOT(getThreadMessage(QString)));

    // set widgets read only
    ui->logPlainTextEdit->setReadOnly(true);
    ui->saveAsLineEdit->setReadOnly(true);

    QString path =
            QStandardPaths::
            standardLocations(QStandardPaths::DesktopLocation).at(0);

    setupFileDialog(path);

    // append filename to path
    QDateTime datetime = QDateTime::currentDateTime();
    path += "/" + datetime.toString(tr("yyyy.MM.dd.hhmmss")) +
            QString(".csv");

    // display path in line edit
    ui->saveAsLineEdit->setText(path);
    ui->saveAsLineEdit->setEnabled(false);

    ui->hardStopPushButton->setVisible(false);

    ui->defaultSaveAsPushButton->setVisible(false);

    ui->drop1stCheckBox->setVisible(false);
    ui->resultComboBox->setVisible(false);
    ui->resultLabel->setVisible(false);
    ui->sampleSizeLabel->setVisible(false);
    ui->sampleSizeSpinBox->setVisible(false);
    ui->line_3->setVisible(false);
}

void MainWindow::scanSerialPorts()
{
    ui->SrComPortComboBox->clear();
    ui->VchComPortComboBox->clear();

    // enumerate serial ports and display them in comboboxes
    QList<QSerialPortInfo> list = QSerialPortInfo::availablePorts();
    for (int i = 0; i < list.count(); ++i) {
        QSerialPortInfo spi = list.at(i);
        ui->SrComPortComboBox->addItem(spi.portName());
        ui->VchComPortComboBox->addItem(spi.portName());
    }
}

void MainWindow::setupListWidget()
{
    ui->inChannelsListWidget->clear();

    // add 10 checked channel items to the list widget
    for (int i = 1; i <= 10; i++)
    {
        QListWidgetItem* item = new QListWidgetItem(QString("Канал%1").arg(i),
                                                    ui->inChannelsListWidget);

        item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
        item->setCheckState(Qt::Checked);
    }
}

void MainWindow::setupFileDialog(const QString &directory)
{
    fileDialog.setDirectory(directory);
    fileDialog.setFileMode(QFileDialog::Directory);
    fileDialog.setOption(QFileDialog::ShowDirsOnly, true);
    fileDialog.setWindowTitle(tr("Выбрать папку.."));
    fileDialog.setAcceptMode(QFileDialog::AcceptOpen);
}

void MainWindow::timer1_work()
{
    ui->progressBar->stepUp();
}

void MainWindow::getThreadMessage(QString str)
{
    ui->logPlainTextEdit->moveCursor(QTextCursor::End);
    ui->logPlainTextEdit->insertPlainText(str);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete timer1;

    if (!mythread.isFinished()) {
        emit finishThread();
        mythread.wait();
    }
}

void MainWindow::setWidgetsEnabled(bool enabled)
{
    ui->startButton->setEnabled(enabled);
    ui->stopButton->setEnabled(!enabled);

    ui->inChannelsListWidget->setEnabled(enabled);

    ui->saveAsPushButton->setEnabled(enabled);

    ui->SrComPortComboBox->setEnabled(enabled);
    ui->VchComPortComboBox->setEnabled(enabled);

    ui->outChannelSpinBox->setEnabled(enabled);
    ui->scanSerialPortsButton->setEnabled(enabled);
}

void MainWindow::on_startButton_clicked()
{
    if (mythread.isRunning())
        return;

    if (ui->SrComPortComboBox->currentText()
            == ui->VchComPortComboBox->currentText()) {
        QMessageBox::critical(this, tr("Ошибка"),
                        tr("Порты приборов не заданы или конфликтуют"));
        return;
    }

    // regenerate output filename and display it
    QDir dir(ui->saveAsLineEdit->text());
    dir.cdUp();
    QString path = dir.path();
    path += "/" +
            QDateTime::currentDateTime().toString(tr("yyyy.MM.dd.hhmmss")) +
            QString(".txt");

    ui->saveAsLineEdit->setText(path);

    // set parameters
    mythread.setOutFilename(ui->saveAsLineEdit->text());

    mythread.setComPortNames(ui->SrComPortComboBox->currentText(),
                             ui->VchComPortComboBox->currentText());

    mythread.setVchChannels(ui->inChannelsListWidget,
                            ui->outChannelSpinBox->text().toInt());

    mythread.start();
    qDebug("start thread");

    timer1->start(20);
    qDebug("start timer");

    setWidgetsEnabled(false);
}

void MainWindow::on_stopButton_clicked()
{
    if (mythread.isRunning()) {
        setCursor(Qt::WaitCursor);
        ui->stopButton->setEnabled(false);
        emit finishThread();
        qDebug("finish thread");
    }
}

void MainWindow::threadFinished()
{
    qDebug("thread finished");
    ui->progressBar->reset();

    if (timer1->isActive()) {
        timer1->stop();
        qDebug("stop timer");
    }

    setWidgetsEnabled(true);

    setCursor(Qt::ArrowCursor);
}

void MainWindow::on_saveAsPushButton_clicked()
{
    if (fileDialog.exec()) {
        ui->saveAsLineEdit->setText(fileDialog.selectedFiles().at(0));
        QString path = ui->saveAsLineEdit->text();
        path += "/" +
            QDateTime::currentDateTime().toString(tr("yyyy.MM.dd.hhmmss")) +
            QString(".txt");
        ui->saveAsLineEdit->setText(path);
    }
}

void MainWindow::on_scanSerialPortsButton_clicked()
{
    scanSerialPorts();
}
