#-------------------------------------------------
#
# Project created by QtCreator 2016-08-24T16:01:35
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = timefreaq
TEMPLATE = app


SOURCES += main.cpp\
    mainwindow.cpp \
    mythread.cpp \
    myprogressbar.cpp \
    serial-timefreaq/sr620.c \
    serial-timefreaq/vch603.c

HEADERS  += mainwindow.h\
    mythread.h \
    myprogressbar.h \
    serial-timefreaq/sr620.h \
    serial-timefreaq/vch603.h

FORMS    += mainwindow.ui
